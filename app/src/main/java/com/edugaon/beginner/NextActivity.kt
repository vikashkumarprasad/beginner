package com.edugaon.beginner

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button
import android.widget.Toast
import android.widget.ToggleButton
import com.google.android.material.floatingactionbutton.FloatingActionButton

class NextActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_next)
        val myLayoutButton = findViewById<Button>(R.id.myRelativeLayout_button)
        myLayoutButton.setOnClickListener {
            val intent = Intent(this,RelativeLayoutActivity::class.java)
            startActivity(intent)
        }
    }
}