package com.edugaon.beginner

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button

class RelativeLayoutActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_relative_layout)

        val shapeButton = findViewById<Button>(R.id.myShape_button)
        shapeButton.setOnClickListener {
            val intent = Intent(this,ShapesActivity::class.java)
            startActivity(intent)
        }
    }
}